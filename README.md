Installation
------------
Copy the contents of the `FTPFS` directory to a place accessible by php
to allow inclusion from the WebDAV server script.

Usage
-----
Add `require_once 'FTPFS\autoload.php'` into the beginning of you server script.
Then just use the `Sabre\DAV\FTPFS\Directory` when constructing `Sabre\DAV\Server`.

See `Sabre\DAV\FTPFS\Directory` class documentation for more details.
