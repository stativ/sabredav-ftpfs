<?php
/**
 * @copyright Lukas Jirkovsky
 * @author Lukas Jirkovsky (l.jirkovsky@gmail.com)
 * @license BSD 3-Clause License
 *
 * Generate Mime.php class that maps mime types to known extensions
 */

function main() {
    $mimeMap = getMimeTypeMap();
    $handle = fopen('Mime.php', 'w');
    genHeader($handle);
    genBody($handle, $mimeMap);
    genFooter($handle);
    fclose($handle);
}

/**
 * Generate header of the Mime.php file
 *
 * @param handle file handle to write the data to
 */
function genHeader($handle) {
    $header = <<<'EOD'
<?php
/**
 * @author generated using mimegen.php script
 * @license BSD 3-Clause License
 */

namespace Stativ\DAV\FTPFS;

/**
 * Guess mime type from extension.
 */
class Mime {

    /**
     * Get mime type for extension
     *
     * @param $ext extension to check
     * @return the mime type for the extension or null if the extension is not known
     */
    public static function getMimeType($ext){
        if(array_key_exists($ext, self::$MIME_MAP)) {
            return self::$MIME_MAP[$ext];
        }
        return null;
    }


EOD;
    fwrite($handle, $header);
}

/**
 * Generate footer of the Mime.php file
 *
 * @param handle file handle to write the data to
 */
function genFooter($handle) {
    $footer = <<<'EOD'
}
EOD;
    fwrite($handle, $footer);
}

/**
 * Generate body of the Mime.php file
 *
 * @param handle file handle to write the data to
 */
function genBody($handle, $mimeMap) {
    fwrite($handle, "    private static \$MIME_MAP = [\n");

    foreach ($mimeMap as $key => $value) {
        $tmpkey = strtolower($key);
        fwrite($handle, "        \"$tmpkey\" => \"$value\",\n");
    }

    fwrite($handle, "    ];\n");
}

/**
 * Get map that maps extion to mime type
 *
 * @return map of mime types
 */
function getMimeTypeMap() {
    $PATTERN = '/^([^#]\S+)\s+(\S+.*)/';
    $URL = 'http://svn.apache.org/viewvc/httpd/httpd/trunk/docs/conf/mime.types?view=co';

    $map = [];
    $handle = fopen($URL, 'r');
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            if(preg_match($PATTERN, $line, $matches)) {
                foreach (explode(' ', $matches[2]) as $ext) {
                    $map[$ext] = $matches[1];
                }
            }
        }
        fclose($handle);
    }

    return $map;
}

main();
