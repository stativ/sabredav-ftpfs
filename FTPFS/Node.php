<?php
/**
 * @copyright Lukas Jirkovsky
 * @author Lukas Jirkovsky (l.jirkovsky@gmail.com)
 * @license BSD 3-Clause License
 */

namespace Stativ\DAV\FTPFS;

/**
 * RAII class handling connection to FTP.
 *
 * Thanks to reference counting, it also alows sharing of the same connection.
 */
class Connection {
    private $ftpServer;
    private $ftpUserName;
    private $ftpUserPass;
    private $passive;

    private $connId;

    /**
     * Connects to FTP.
     *
     * @param string $ftpServer FTP server to connect to
     * @param string $ftpUserName FTP user name
     * @param string $ftpUserPass FTP user password
     * @param bool $passive If true, FTP is set to passive mode
     */
    public function __construct($ftpServer, $ftpUserName, $ftpUserPass, $passive) {
        $this->ftpServer = $ftpServer;
        $this->ftpUserName = $ftpUserName;
        $this->ftpUserPass = $ftpUserPass;
        $this->passive = $passive;
    
        $this->connect($ftpServer, $ftpUserName, $ftpUserPass, $passive);
    }

    /**
     * Close connection to FTP
     */
    public function __destruct() {
        $this->disconnect();
    }
    
    private function connect($ftpServer, $ftpUserName, $ftpUserPass, $passive) {
        $this->connId = ftp_connect($ftpServer);
        if (!$this->connId) {
            throw new \Sabre\DAV\Exception('Failed to connect to FTP');
        }

        $loginResult = ftp_login($this->connId, $ftpUserName, $ftpUserPass);
        if (!$loginResult) {
            throw new \Sabre\DAV\Exception('FTP login failed');
        }

        if (! ftp_pasv($this->connId, $passive)) {
            throw new \Sabre\DAV\Exception('Failed to set ' . $passive ? 'passive' : 'active' . ' FTP mode');
        }
    }
    
    private function disconnect() {
        ftp_close($this->connId);
    }

    /**
     * Get the opened FTP stream
     */
    public function getStream() {
        return $this->connId;
    }
    
    /**
     * Execute a ftp function and if the function returns FALSE, try to reconnect.
     *
     * This function automatically passes the connection id as the first argument.
     *
     * @param string $func name of the function to execute
     * @param array $args array of the function arguments without the connection id
     * @return the result of calling $func with $args
     */
    public function execRetryOnFalse($func, $args) {
        array_unshift($args, $this->connId);
        $res = call_user_func_array($func, $args);
        if ($res === false) {
            $this->disconnect();
            sleep(1);
            $this->connect($this->ftpServer, $this->ftpUserName, $this->ftpUserPass, $this->passive);
            $args[0] = $this->connId;
            return call_user_func_array($func, $args);
        }
        return $res;
    }
    
    /**
     * Execute a ftp function and if the function returns -1, try to reconnect.
     *
     * This function automatically passes the connection id as the first argument.
     *
     * @param string $func name of the function to execute
     * @param array $args array of the function arguments without the connection id
     * @return the result of calling $func with $args
     */
    public function execRetryOnNegative($func, $args) {
        array_unshift($args, $this->connId);
        $res = call_user_func_array($func, $args);
        if ($res === -1) {
            $this->disconnect();
            $this->connect($this->ftpServer, $this->ftpUserName, $this->ftpUserPass, $this->passive);
            $args[0] = $this->connId;
            return call_user_func_array($func, $args);
        }
        return $res;
    }
}

/**
 * Base node-class
 *
 * The node class implements the methods used by both the File and the Directory classes.
 */
abstract class Node implements \Sabre\DAV\INode {

    protected $path;
    protected $connection;

    /**
     * Prepares Node by creating a new connection or reusing an existing one.
     *
     * @param string $ftpServer FTP server to connect to
     * @param string $ftpUserName FTP user name
     * @param string $ftpUserPass FTP user password
     * @param bool $passive If true, FTP is set to passive mode
     * @param string $ftpPath default FTP directory
     *
     * OR
     *
     * @param Connection $connection Existing connection
     * @param string $ftpPath default FTP directory
     */
    function __construct() {
        switch (func_num_args()) {
            case 2:
                $this->connection = func_get_arg(0);
                $this->path = func_get_arg(1);
                break;
            case 5:
                $this->connection = new Connection(func_get_arg(0), func_get_arg(1), func_get_arg(2), func_get_arg(3));
                $this->path = func_get_arg(4);
                break;
            default:
                throw new \Sabre\DAV\Exception('Invalid number of arguments passed to FTPFS\Node constructor');
        }
    }
    
    /**
     * Returns the ETag for a file
     *
     * @param string $file path to file
     * @return ETag or null if it's not possible to create ETag
     */
    protected function computeETag($file) {
        $mtime = $this->connection->execRetryOnNegative("ftp_mdtm", [$file]);
        if($mtime == -1) {
            return null;
        }
        $size = $this->connection->execRetryOnNegative("ftp_size", [$file]);
        if($size == -1) {
            return null;
        }
        return '"' . sha1($mtime . $size) . '"';
    }

    /*******************
     * INode interface *
     *******************/

    /**
     * Returns the name of the node
     *
     * @return string
     */
    function getName() {
        list(, $name)  = \Sabre\HTTP\URLUtil::splitPath($this->path);
        return $name;
    }

    /**
     * Renames the node
     *
     * @param string $name The new name
     * @return void
     */
    function setName($name) {
        list($parentPath, ) = \Sabre\HTTP\URLUtil::splitPath($this->path);
        list(, $newName) = \Sabre\HTTP\URLUtil::splitPath($name);

        $newPath = $parentPath . '/' . $newName;
        $this->connection->execRetryOnFalse("ftp_rename", [$this->path, $newPath]);

        $this->path = $newPath;
    }

    /**
     * Returns the last modification time, as a unix timestamp
     *
     * @return int
     */
    function getLastModified() {
        return $this->connection->execRetryOnNegative("ftp_mdtm", [$this->path]);
    }
}
