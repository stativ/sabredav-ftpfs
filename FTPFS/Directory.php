<?php
/**
 * @copyright Lukas Jirkovsky
 * @author Lukas Jirkovsky (l.jirkovsky@gmail.com)
 * @license BSD 3-Clause License
 */

namespace Stativ\DAV\FTPFS;

/**
 * Directory implementation for directories on FTP.
 *
 * Because FTP does not offer a way to get quota information, the
 * class does _not_ implement IQuota interface.
 */
class Directory extends Node implements \Sabre\DAV\ICollection {
    /*******************
     * INode interface *
     *******************/

    /**
     * Deletes all files in this directory, and then itself
     *
     * @return void
     */
    function delete() {
        foreach ($this->getChildren() as $child) {
            $child->delete();
        }
        $this->connection->execRetryOnFalse("ftp_rmdir", [$this->path]);
    }

    /*************************
     * ICollection interface *
     *************************/

    /**
     * Creates a new file in the directory
     *
     * First a temporary file is created. This file is then uploaded to FTP server.
     *
     * @param string $name Name of the file
     * @param resource|string $data Initial payload
     * @return ETag or null if it's not possible to create ETag
     */
    function createFile($name, $data = null) {
        $path = $this->path . '/' . $name;
        if(is_resource($data)) {
            # assumes that if $data is a resource, it is a readable stream
            $this->connection->execRetryOnFalse("ftp_fput", [$path, $data, FTP_BINARY]);
        }
        else {
            $tmpFile = tmpfile();
            fwrite($tmpFile, $data);
            rewind($tmpFile);
            $this->connection->execRetryOnFalse("ftp_fput", [$path, $tmpFile, FTP_BINARY]);
            fclose($tmpFile);
        }
        return $this->computeETag($path);
    }

    /**
     * Creates a new subdirectory
     *
     * @param string $name
     * @return void
     */
    function createDirectory($name) {
        $path = $this->path . '/' . $name;
        $this->connection->execRetryOnFalse("ftp_mkdir", [$path]);
    }

    /**
     * Returns a specific child node, referenced by its name
     *
     * This method must throw DAV\Exception\NotFound if the node does not
     * exist.
     *
     * @param string $name
     * @throws DAV\Exception\NotFound
     * @return DAV\INode
     */
    function getChild($name) {
        $path = $this->path . '/' . $name;

        if(!$this->childExists($name)) {
            throw new \Sabre\DAV\Exception\NotFound('File with name ' . $path . ' could not be located');
        }

        if($this->connection->execRetryOnNegative("ftp_size", [$path]) >= 0) {
            return new File($this->connection, $path);
        }
        else if($this->connection->execRetryOnFalse("ftp_chdir", [$path])) {
            $this->connection->execRetryOnFalse("ftp_chdir", [".."]);
            return new Directory($this->connection, $path);
        }
        else {
            throw new \Sabre\DAV\Exception\NotFound('Cannot determine whether ' . $path . ' if file or directory');
        }
    }

    /**
     * Returns an array with all the child nodes
     *
     * @return DAV\INode[]
     */
    function getChildren() {
        $nodes = [];
        $ftpList = $this->connection->execRetryOnFalse("ftp_nlist", [$this->path]);

        foreach ($ftpList as $entry) {
            // ftp_nlist returns full paths, but we want the last part only...
            list(, $fileName)  = \Sabre\HTTP\URLUtil::splitPath($entry);
            $nodes[] = $this->getChild($fileName);
        }
        return $nodes;
    }

    /**
     * Checks if a child exists.
     *
     * @param string $name
     * @return bool
     */
    function childExists($name) {
        $path = $this->path . '/' . $name;
        $ftpList = $this->connection->execRetryOnFalse("ftp_nlist", [$this->path]);
        return in_array($path, $ftpList);
    }
}
