<?php
/**
 * @copyright Lukas Jirkovsky
 * @author Lukas Jirkovsky (l.jirkovsky@gmail.com)
 * @license BSD 3-Clause License
 */

namespace Stativ\DAV\FTPFS;

/**
 * File implementation for files on FTP.
 */
class File extends Node implements \Sabre\DAV\IFile {

    /**
     * Updates the data
     *
     * @param resource $data
     * @return void
     */
    function put($data) {
        $this->connection->execRetryOnFalse("ftp_fput", [$this->path, $data, FTP_BINARY]);
    }

    /**
     * Returns the data
     *
     * @return resource
     */
    function get() {
        $tmpFile = tmpfile();
        $this->connection->execRetryOnFalse("ftp_fget", [$tmpFile, $this->path, FTP_BINARY]);
        rewind($tmpFile);
        return $tmpFile;
    }

    /**
     * Delete the current file
     *
     * @return void
     */
    function delete() {
        $this->connection->execRetryOnFalse("ftp_delete", [$this->path]);
    }

    /**
     * Returns the size of the node, in bytes
     *
     * @return int
     */
    function getSize() {
        return $this->connection->execRetryOnNegative("ftp_size", [$this->path]);
    }

    /**
     * Returns the ETag for a file
     *
     * @return ETag or null if it's not possible to create ETag
     */
    function getETag() {
        return $this->computeETag($this->path);
    }

    /**
     * Returns the mime-type for a file
     *
     * The mime type is guessed based on the file extension.
     * If it's not possible to detect the mime type, the function
     * falls back to returning null
     *
     * @return the mime type of file or null if the mime type cannot
               be determined
     */
    function getContentType() {
        $ext = pathinfo($this->path, PATHINFO_EXTENSION);
        $mime = Mime::getMimeType(strtolower($ext));
        if (!empty($mime)) {
            return $mime;
        }
        // mime type cannot be guessed from the extension
        return null;
    }
}
